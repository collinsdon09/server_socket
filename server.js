const fs = require("fs");
const server = require("http").createServer();
const io = require("socket.io")(server);
const cors = require("cors");



io.use(cors());

io.on("connection", (socket) => {
  socket.on("startListening", () => {
    // Read the audio file as a blob
    const audioFilePath = "4.mp3";
    const audioBlob = fs.readFileSync(audioFilePath);

    // Emit the audio blob to the client
    socket.emit("audio", audioBlob);
  });
});

server.listen(4000, () => {
  console.log("Server is running on port 4000");
});
